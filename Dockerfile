FROM docker:latest

MAINTAINER geometalab <geometalab@hsr.ch>

RUN apk --update add \
    bash \
    python2 \
    python2-dev \
    py2-pip \
    python3 \
    python3-dev \
    py3-pip \
    wget \
    gcc \
    g++ \
    make \
    && \
    \
    rm -rf /var/cache/apk/*

# make -j$(nproc)

ENV NODE_VERSION v8.4.0

RUN apk --update add \
    linux-headers && \
    wget https://nodejs.org/dist/$NODE_VERSION/node-$NODE_VERSION.tar.gz && \
    tar xf node-$NODE_VERSION.tar.gz && \
    cd node-$NODE_VERSION && \
    ./configure && \
    make -j6 && \
    make install && \
    rm -rf node-$NODE_VERSION.tar.gz node-$NODE_VERSION/ && \
    apk --update del linux-headers && \
    rm -rf /var/cache/apk/*

RUN pip install docker-compose
RUN npm rebuild node-sass
